<?php


// =====================================================================
// Function which displays the graphic bar.
// =====================================================================
function graphic_bar ($size_new_graphic, $size_old_graphic) {


  // ===================================================================
  // Limit the new graphic to 100 %
  // ===================================================================
  if ($size_new_graphic > 200) {
    $size_new_graphic = 200;
  }


  // ===================================================================
  // Graphic table begin.
  // ===================================================================
  echo "<table class=\"graphic\" border=\"0\" width=\"240\" cellspacing=\"0\" cellpadding=\"0\">\n";


  // ===================================================================
  // Graphic bar.
  // ===================================================================
  echo "<tr>\n";
  echo "<td class=\"graphic-bar\">\n";
  echo "<img src=\"images/graphic_space.gif\" width=\"19\" height=\"34\" border=\"0\" alt=\"\">";
  echo "<img src=\"images/graphic_border.gif\" width=\"1\" height=\"34\" border=\"0\" alt=\"\">";
  echo "<img src=\"images/graphic_new.gif\" width=\"".$size_new_graphic."\" height=\"34\" border=\"0\" alt=\"Percent New\">";


  // ===================================================================
  // Check that the graphic is bigger than 0.
  // ===================================================================
  if ($size_old_graphic != 0) {
    echo "<img src=\"images/graphic_old.gif\" width=\"".$size_old_graphic."\" height=\"34\" border=\"0\" alt=\"Percent Old\">";
  }
  echo "<img src=\"images/graphic_border.gif\" width=\"1\" height=\"34\" border=\"0\" alt=\"\">\n";
  echo "</td>\n";
  echo "</tr>\n";


  // ===================================================================
  // Graphic scale.
  // ===================================================================
  echo "<tr>\n";
  echo "<td class=\"graphic-scale\">\n";
  echo "</td>\n";
  echo "</tr>\n";


  // ===================================================================
  // Graphic table end.
  // ===================================================================
  echo "</table>\n";
}


// =====================================================================
// Function which prepares a real path string.
// =====================================================================
function real_path ($path) {


  // ===================================================================
  // Remove double slashes from the path.
  // ===================================================================
  str_replace("//","/",$path);


  // ===================================================================
  // If the path has an end slash remove it.
  // ===================================================================
  if (substr(trim($path), -1) == "/") {
    $path = substr(trim($path), 0, -1);
  }


  // ===================================================================
  // Get the absolute path and trim it.
  // ===================================================================
  $path = realpath(trim($path));


  // ===================================================================
  // Return the path to the caller.
  // ===================================================================
  return $path;
}


// =====================================================================
// Function which gets information about a directory.
//
// $startdir = startdirectory, can be relative or absolut
// $subdir   = number of levels to scan, 0 is just the startdir
// $type     = array of fileendings from which to get information
//
// Example: $status = directory_info ("../test", 3, $type);
//
// =====================================================================
function directory_info ($startdir, $subdir, $filetype, $excludefile) {


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  global $directory;
  global $processfile;
  global $copyfile;
  global $basedir;
  $fail = 0;


  // ===================================================================
  // Remove double slashes from the path.
  // ===================================================================
  str_replace("//","/",$startdir);


  // ===================================================================
  // Get the absolute path and trim it.
  // ===================================================================
  $startdir = realpath(trim($startdir));


  // ===================================================================
  // If the path has an end slash remove it.
  // ===================================================================
  if (substr(trim($startdir), -1) == "/") {
    $startdir = substr(trim($startdir), 0, -1);
  }


  // ===================================================================
  // Check if the directory exists.
  // ===================================================================
  if (is_dir($startdir)) {


    // =================================================================
    // Set the base directory if it is not allready set.
    // =================================================================
    if (!isset($basedir)) {
      $basedir = $startdir;
    }


    // =================================================================
    // Calculate the directory level.
    // =================================================================
    $level = substr_count($startdir,"/") - substr_count($basedir,"/");


    // =================================================================
    // Open the directory.
    // =================================================================
    if ($dirhandler = opendir($startdir)) {


      // ===============================================================
      // Get the relative directory path.
      // ===============================================================
      $rel_dir = substr($startdir, strlen($basedir));


      // ===============================================================
      // Loop through the directory.
      // ===============================================================
      while($entry = readdir($dirhandler)) {


        // =============================================================
        // Exclude unix directory dots and check the fileending.
        // =============================================================
        if(trim($entry) != "." && trim($entry) != "..") {


          // ===========================================================
          // Check if the entry is a directory.
          // ===========================================================
          if (is_dir($startdir."/".$entry)) {


            // =========================================================
            // Store the directory information.
            // =========================================================
            $directory['type'][]     = 0;
            $directory['name'][]     = $entry;
            $directory['abs_dir'][]  = $startdir;
            $directory['rel_dir'][]  = $rel_dir;
            $directory['abs_path'][] = $startdir."/".$entry;
            $directory['rel_path'][] = $rel_dir."/".$entry;
            $directory['level'][]    = $level;


            // =========================================================
            // Check if the next level should be processed.
            // =========================================================
            if ($level < $subdir) {


              // =======================================================
              // Get the information inside the directory.
              // =======================================================
              directory_info ($startdir."/".$entry, $subdir, $filetype, $excludefile);


              // =======================================================
              // If there is an error break the loop.
              // =======================================================
              if ($fail != 0) {
                break;
              }
            }


          // ===========================================================
          // Check if the entry is a file.
          // ===========================================================
          } elseif (is_file($startdir."/".$entry)) {


            // =========================================================
            // Get the filetype from the actual file.
            // =========================================================
            $position = strrpos($entry, ".");
            $actual_type = substr($entry, $position+1);


            // =========================================================
            // Check the fileending.
            // =========================================================
            if(in_array($actual_type, $filetype) && !in_array($entry, $excludefile) && !in_array($rel_dir."/".$entry, $excludefile)) {


              // =======================================================
              // Get the number of lines from the actual file.
              // =======================================================
              $content = file($startdir."/".$entry);
              $line = count($content);


              // =======================================================
              // Unset the variable with the original code.
              // =======================================================
              unset($content);


              // =======================================================
              // Store the file information.
              // =======================================================
              $processfile['type'][]     = 1;
              $processfile['name'][]     = $entry;
              $processfile['abs_dir'][]  = $startdir;
              $processfile['rel_dir'][]  = $rel_dir;
              $processfile['abs_path'][] = $startdir."/".$entry;
              $processfile['rel_path'][] = $rel_dir."/".$entry;
              $processfile['level'][]    = $level;
              $processfile['size_b'][]   = filesize($startdir."/".$entry);
              $processfile['size_kb'][]  = round((filesize($startdir."/".$entry) / 1024), 2);
              $processfile['fileend'][]  = $filetype;
              $processfile['line'][]     = $line;


            // =========================================================
            // Add the file information to the copy array.
            // =========================================================
            } else {


              // =======================================================
              // Store the file information to the copy array.
              // =======================================================
              $copyfile['name'][]     = $entry;
              $copyfile['abs_dir'][]  = $startdir;
              $copyfile['rel_dir'][]  = $rel_dir;
              $copyfile['abs_path'][] = $startdir."/".$entry;
              $copyfile['rel_path'][] = $rel_dir."/".$entry;
              $copyfile['fileend'][]  = $filetype;
            }
          }
        }
      }


      // ===============================================================
      // Close the directory.
      // ===============================================================
      closedir($dirhandler);


    // =================================================================
    // The start directory could not be opened.
    // =================================================================
    } else {
      echo "The directory ".$startdir." could not be opened !<br>";
      $fail++;
    }


  // ===================================================================
  // The start directory does not exist.
  // ===================================================================
  } else {
    echo $startdir." is not a directory !<br>";
    $fail++;
  }


  // ===================================================================
  // Return the status to the caller.
  // ===================================================================
  if ($fail == 0) {
    $status = 1;
  } else {
    $status = 0;
  }
  return $status;
}


// =====================================================================
// Function which creates a directory structure.
//
// $startdir  = startdirectory, can be relative or absolut
// $directory = directory array from the function directory_info()
//
// Example: $status = directory_create ("../test", $directory);
//
// =====================================================================
function directory_create ($startdir, $directory, $copyfile) {


  // ===================================================================
  // Initialize the error flag.
  // ===================================================================
  $fail = 0;


  // ===================================================================
  // Remove double slashes from the path.
  // ===================================================================
  str_replace("//","/",$startdir);


  // ===================================================================
  // Get the absolute path and trim it.
  // ===================================================================
  $startdir = realpath(trim($startdir));


  // ===================================================================
  // Check if the start directory exists.
  // ===================================================================
  if (file_exists($startdir)) {


    // =================================================================
    // Loop through the directories.
    // =================================================================
    for ($dir_counter = 0; $dir_counter < count($directory['rel_path']); $dir_counter++ ) {


      // ===============================================================
      // Create the directory path.
      // ===============================================================
      $basedir = $startdir.$directory['rel_path'][$dir_counter];


      // ===============================================================
      // Check if the directory not allready exists.
      // ===============================================================
      if (!file_exists($basedir)) {


        // =============================================================
        // Check if the start directory is writeable.
        // =============================================================
        if (is_writable($startdir)) {


          // ===========================================================
          // Check if the directory was successfully created.
          // ===========================================================
          if (!mkdir($basedir)) {


            // =========================================================
            // Error while creating the directory.
            // =========================================================
            echo "CD: Error while creating the directory ".$basedir." !<br>";
            $fail++;
          }


        // =============================================================
        // The start directory is not writeable.
        // =============================================================
        } else {
          echo "CD: The directory ".$startdir." is not writable !<br>";
          $fail++;
          break;
        }
      }
    }


  // ===================================================================
  // The start directory does not exist.
  // ===================================================================
  } else {
    echo "CD: Directory ".$startdir." does not exist !<br>";
    $fail++;
  }


  // ===================================================================
  // Check if there are files to copy and if the start directory exist.
  // ===================================================================
  if(count($copyfile) > 0 && file_exists($startdir)) {


    // =================================================================
    // Loop through the copy files.
    // =================================================================
    for ($file_counter = 0; $file_counter < count($copyfile['rel_path']); $file_counter++ ) {


      // ===============================================================
      // Create the directory path.
      // ===============================================================
      $basedir = $startdir.$copyfile['rel_dir'][$file_counter];
      $basepath = $startdir.$copyfile['rel_path'][$file_counter];


      // ===============================================================
      // Check if the base directory exists.
      // ===============================================================
      if (file_exists($basedir)) {


        // =============================================================
        // Check if the base directory is writeable.
        // =============================================================
        if (is_writable($basedir)) {


          // ===========================================================
          // Check if the file was successfully copied.
          // ===========================================================
          if (!copy($copyfile['abs_path'][$file_counter], $basepath)) {


            // =========================================================
            // Error while copying the file.
            // =========================================================
            echo "CF: Error while copying the file ".$copyfile['abs_path'][$file_counter]." !<br>";
            $fail++;
          }


        // =============================================================
        // The destination directory is not writeable.
        // =============================================================
        } else {
          echo "CF: The directory ".$basedir." is not writable !<br>";
          $fail++;
          break;
        }


      // ===============================================================
      // The destination directory does not exist.
      // ===============================================================
      } else {
        echo "CF: The destination directory ".$basedir." does not exist !<br>";
        $fail++;
        break;
      }
    }
  }


  // ===================================================================
  // Return the status to the caller.
  // ===================================================================
  if ($fail == 0) {
    $status = 1;
  } else {
    $status = 0;
  }
  return $status;
}


// =====================================================================
// Function which sorts the information arrays.
// =====================================================================
function sort_info () {


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  global $directory;
  global $processfile;
  global $copyfile;


  // ===================================================================
  // Sort the directory array if the array contains data.
  // ===================================================================
  if (count($directory) > 0) {
    array_multisort(
      $directory['type'],
      $directory['name'],
      $directory['abs_dir'],
      $directory['rel_dir'],
      $directory['abs_path'],
      $directory['rel_path'],
      $directory['level']
    );
  }


  // ===================================================================
  // Sort the processfile array if the array contains data.
  // ===================================================================
  if (count($processfile) > 0) {
    array_multisort(
      $processfile['type'],
      $processfile['name'],
      $processfile['abs_dir'],
      $processfile['rel_dir'],
      $processfile['abs_path'],
      $processfile['rel_path'],
      $processfile['level'],
      $processfile['size_b'],
      $processfile['size_kb'],
      $processfile['fileend'],
      $processfile['line']
    );
  }


  // ===================================================================
  // Sort the copyfile array if the array contains data.
  // ===================================================================
  if (count($copyfile) > 0) {
    array_multisort(
      $copyfile['name'],
      $copyfile['abs_dir'],
      $copyfile['rel_dir'],
      $copyfile['abs_path'],
      $copyfile['rel_path'],
      $copyfile['fileend']
    );
  }
}
