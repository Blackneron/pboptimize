<?php


// =====================================================================
// Display the form begin.
// =====================================================================
echo "<form name=\"main_form\" id=\"main_form\" action=\"pboptimize.php\" method=\"post\">\n";


// =====================================================================
// Display a space.
// =====================================================================
echo "<br>\n";


// =====================================================================
// Display the main table begin.
// =====================================================================
echo "<table class=\"main\" border=\"0\" width=\"840\" cellspacing=\"0\" cellpadding=\"0\">\n";


// =====================================================================
// Display the title row.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"main-title\" colspan=\"5\">\n";
echo count($s_processfile['name'])." ".stripslashes($maintable[0])."\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the source directory row.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"main-source-directory\" colspan=\"5\">\n";
echo stripslashes($maintable[1])." ".$source_dir."\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Check if there are files to display.
// =====================================================================
if (count($s_processfile['name']) > 0) {


  // ===================================================================
  // Display the header row.
  // ===================================================================
  echo "<tr>\n";
  echo "<td class=\"main-header1\">\n";
  echo stripslashes($maintable[2])."\n";
  echo "</td>\n";
  echo "<td class=\"main-header2\">\n";
  echo stripslashes($maintable[3])."\n";
  echo "</td>\n";
  echo "<td class=\"main-header3\">\n";
  echo stripslashes($maintable[4])."\n";
  echo "</td>\n";
  echo "<td class=\"main-header4\">\n";
  echo stripslashes($maintable[5])."\n";
  echo "</td>\n";
  echo "<td class=\"main-header5\">\n";
  echo stripslashes($maintable[6])."\n";
  echo "</td>\n";
  echo "</tr>\n";


  // ===================================================================
  // We make a loop to show the files of the source directory.
  // ===================================================================
  for($counter = 0; $counter < count($s_processfile['name']); $counter++) {


    // =================================================================
    // If the relative directory path is empty use a placeholder.
    // =================================================================
    if ($s_processfile['rel_dir'][$counter] == "") {
      $s_processfile['rel_dir'][$counter] = "&nbsp;";
    }


    // =================================================================
    // Display the actual file.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"main-col1\">\n";
    echo ($counter+1)."\n";
    echo "</td>\n";
    echo "<td class=\"main-col2\">\n";
    echo $s_processfile['name'][$counter]."\n";
    echo "</td>\n";
    echo "<td class=\"main-col3\">\n";
    echo $s_processfile['rel_dir'][$counter]."\n";
    echo "</td>\n";
    echo "<td class=\"main-col4\">\n";
    echo $s_processfile['line'][$counter]."\n";
    echo "</td>\n";
    echo "<td class=\"main-col5\">\n";
    echo sprintf("%01.2f", $s_processfile['size_kb'][$counter])." KB<br>\n";
    echo "</td>\n";
    echo "</tr>\n";
  }


  // ===================================================================
  // Display the total row.
  // ===================================================================
  echo "<tr>\n";
  echo "<td class=\"main-footer1\">\n";
  echo count($s_processfile['name'])."\n";
  echo "</td>\n";
  echo "<td class=\"main-footer2\">\n";
  echo "&nbsp;\n";
  echo "</td>\n";
  echo "<td class=\"main-footer3\">\n";
  echo "&nbsp;\n";
  echo "</td>\n";
  echo "<td class=\"main-footer4\">\n";
  echo array_sum($s_processfile['line'])."<br>\n";
  echo "</td>\n";
  echo "<td class=\"main-footer5\">\n";
  echo sprintf("%01.2f", array_sum($s_processfile['size_kb']))." KB<br>\n";
  echo "</td>\n";
  echo "</tr>\n";


  // ===================================================================
  // Display the form button.
  // ===================================================================
  echo "<tr>\n";
  echo "<td class=\"main-button\" colspan=\"5\">\n";
  echo "<input type=\"submit\" name=\"optimize\" value=\"".stripslashes($maintable[7])."\">\n";
  echo "</td>\n";
  echo "</tr>\n";


// =====================================================================
// Display a message that there are no files available.
// =====================================================================
} else {


  // ===================================================================
  // Display the message that there are no files available.
  // ===================================================================
  echo "<tr>\n";
  echo "<td class=\"main-info\" colspan=\"5\">\n";
  echo stripslashes($maintable[8])."\n";
  echo "</td>\n";
  echo "</tr>\n";
}


// =====================================================================
// Display the main table end.
// =====================================================================
echo "</table>\n";


// =====================================================================
// Display the form end.
// =====================================================================
echo "</form>\n";
