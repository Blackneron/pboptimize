<?php


// =====================================================================
// Calculate filesize in bytes.
// =====================================================================
$source_filesize_byte = array_sum($s_processfile['size_b']);
$destination_filesize_byte = array_sum($d_processfile['size_b']);
$difference_filesize_byte = $destination_filesize_byte - $source_filesize_byte;


// =====================================================================
// Calculate filesize in kilobytes.
// =====================================================================
$source_filesize_kbyte = array_sum($s_processfile['size_kb']);
$destination_filesize_kbyte = array_sum($d_processfile['size_kb']);
$difference_filesize_kbyte = $destination_filesize_kbyte - $source_filesize_kbyte;


// =====================================================================
// Calculate filesize in percent.
// =====================================================================
$size_new_percent = round(($destination_filesize_byte / $source_filesize_byte * 100), 2);
$size_dif_percent = (100 - $size_new_percent);
if ($size_dif_percent != 0) {
  $size_dif_percent = $size_dif_percent * -1;
}


// =====================================================================
// Calculate the graphics for the filesize in percent.
// =====================================================================
$size_new_graphic = floor($destination_filesize_byte / $source_filesize_byte * 200);
$size_old_graphic = 200 - $size_new_graphic;


// =====================================================================
// Calculate the number of code lines.
// =====================================================================
$source_line = array_sum($s_processfile['line']);
$destination_line = array_sum($d_processfile['line']);
$difference_line = $destination_line - $source_line;


// =====================================================================
// Calculate the number of code lines in percent.
// =====================================================================
$line_new_percent = round(($destination_line / $source_line * 100), 2);
$line_dif_percent = (100 - $line_new_percent);
if ($line_dif_percent != 0) {
  $line_dif_percent = $line_dif_percent * -1;
}


// =====================================================================
// Calculate the graphics for the number of code lines in percent.
// =====================================================================
$line_new_graphic = floor($destination_line / $source_line * 200);
$line_old_graphic = 200 - $line_new_graphic;


// =====================================================================
// Check if the script type is specified.
// =====================================================================
if(isset($script_type)) {


  // ===================================================================
  // Show a message about the example version.
  // ===================================================================
  if($script_type == "example") {
    echo "<p class=\"warning\">\n";
    echo $warning[0]."<br>\n";
    echo "<br>\n";
    echo $warning[1]."<br>\n";
    echo "<br>\n";
    echo "</p>\n";
  }
}


// =====================================================================
// Display the information table begin.
// =====================================================================
echo "<table class=\"project-info\" border=\"0\" width=\"760\" cellspacing=\"0\" cellpadding=\"0\">\n";


// =====================================================================
// Display the title row.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-title\" colspan=\"5\">\n";
echo stripslashes($projectinfo[0])." - ".count($s_processfile['name'])." ".stripslashes($projectinfo[1])." - ".stripslashes($projectinfo[2])." ".$size_dif_percent." %\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the header row.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-header1\">\n";
echo stripslashes($projectinfo[3])."\n";
echo "</td>\n";
echo "<td class=\"project-info-header2\">\n";
echo "<a href=\"".$link_source_dir."\" target=\"_blank\">\n";
echo stripslashes($projectinfo[4])."\n";
echo "</a>\n";
echo "</td>\n";
echo "<td class=\"project-info-header3\">\n";
echo "<a href=\"".$link_destination_dir."\" target=\"_blank\">\n";
echo stripslashes($projectinfo[5])."\n";
echo "</a>\n";
echo "</td>\n";
echo "<td class=\"project-info-header4\">\n";
echo stripslashes($projectinfo[6])."\n";
echo "</td>\n";
echo "<td class=\"project-info-header5\">\n";
echo stripslashes($projectinfo[7])."\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the data row - filesize in bytes.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-col1\">\n";
echo stripslashes($projectinfo[8])."\n";
echo "</td>\n";
echo "<td class=\"project-info-col2\">\n";
echo $source_filesize_byte."\n";
echo "</td>\n";
echo "<td class=\"project-info-col3\">\n";
echo $destination_filesize_byte."\n";
echo "</td>\n";
echo "<td class=\"project-info-col4\">\n";
echo $difference_filesize_byte."\n";
echo "</td>\n";
echo "<td class=\"project-info-col5\" rowspan=\"2\">\n";
graphic_bar ($size_new_graphic, $size_old_graphic);
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the data row - filesize in kilobytes.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-col1\">\n";
echo stripslashes($projectinfo[9])."\n";
echo "</td>\n";
echo "<td class=\"project-info-col2\">\n";
echo sprintf("%01.2f", $source_filesize_kbyte)."\n";
echo "</td>\n";
echo "<td class=\"project-info-col3\">\n";
echo sprintf("%01.2f", $destination_filesize_kbyte)."\n";
echo "</td>\n";
echo "<td class=\"project-info-col4\">\n";
echo sprintf("%01.2f", $difference_filesize_kbyte)."\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the data row - filesize in percent.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-col1\">\n";
echo stripslashes($projectinfo[10])."\n";
echo "</td>\n";
echo "<td class=\"project-info-col2\">\n";
echo "100\n";
echo "</td>\n";
echo "<td class=\"project-info-col3\">\n";
echo $size_new_percent."\n";
echo "</td>\n";
echo "<td class=\"project-info-col4\">\n";
echo $size_dif_percent."\n";
echo "</td>\n";
echo "<td class=\"project-info-header5\">\n";
echo stripslashes($projectinfo[11])."\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the data row - number of lines.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-col1\">\n";
echo stripslashes($projectinfo[12])."\n";
echo "</td>\n";
echo "<td class=\"project-info-col2\">\n";
echo $source_line."\n";
echo "</td>\n";
echo "<td class=\"project-info-col3\">\n";
echo $destination_line."\n";
echo "</td>\n";
echo "<td class=\"project-info-col4\">\n";
echo $difference_line."\n";
echo "</td>\n";
echo "<td class=\"project-info-col5\" rowspan=\"2\">\n";
graphic_bar ($line_new_graphic, $line_old_graphic);
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the data row - number of lines in percent.
// =====================================================================
echo "<tr>\n";
echo "<td class=\"project-info-col1\">\n";
echo stripslashes($projectinfo[13])."\n";
echo "</td>\n";
echo "<td class=\"project-info-col2\">\n";
echo "100\n";
echo "</td>\n";
echo "<td class=\"project-info-col3\">\n";
echo $line_new_percent."\n";
echo "</td>\n";
echo "<td class=\"project-info-col4\">\n";
echo $line_dif_percent."\n";
echo "</td>\n";
echo "</tr>\n";


// =====================================================================
// Display the table end.
// =====================================================================
echo "</table>\n";


// =====================================================================
// Display a space.
// =====================================================================
echo "<br>\n";
