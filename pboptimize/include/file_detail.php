<?php


// =====================================================================
// Loop through the files.
// =====================================================================
for ($file_counter = 0; $file_counter < count($s_processfile['name']); $file_counter++) {


  // ===================================================================
  // Check if the filesize is not zero.
  // ===================================================================
  if ($s_processfile['size_b'][$file_counter] != 0) {


    // =================================================================
    // Insert an anchor for the link from the file info table.
    // =================================================================
    echo "<a name=\"".$s_processfile['name'][$file_counter]."\">\n";
    echo "</a>\n";


    // =================================================================
    // Display the table begin.
    // =================================================================
    echo "<table class=\"file-detail\" border=\"0\" width=\"1200\" cellspacing=\"0\" cellpadding=\"0\">\n";


    // =================================================================
    // Display the title row with a link to the top.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-detail-title\" colspan=\"6\">\n";
    echo stripslashes($filedetail[0])." ".$s_processfile['name'][$file_counter]." (";
    echo "<a href=\"#top\">";
    echo stripslashes($filedetail[1]);
    echo "</a>";
    echo ")\n";
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the header row.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-detail-header1\">\n";
    echo stripslashes($filedetail[2])."\n";
    echo "</td>\n";
    echo "<td class=\"file-detail-header2\">\n";
    echo stripslashes($filedetail[3])."\n";
    echo "</td>\n";
    echo "<td class=\"file-detail-header3\">\n";
    echo stripslashes($filedetail[4])."\n";
    echo "</td>\n";
    echo "<td class=\"file-detail-header3\">\n";
    echo stripslashes($filedetail[5])."\n";
    echo "</td>\n";
    echo "<td class=\"file-detail-header4\">\n";
    echo stripslashes($filedetail[6])."\n";
    echo "</td>\n";
    echo "<td class=\"file-detail-header5\">\n";


    // =================================================================
    // Check which code header should be displayed.
    // =================================================================
    if ($show_old_code == 1) {
      echo stripslashes($filedetail[7])."\n";
    } else {
      echo stripslashes($filedetail[8])."\n";
    }
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Loop through the lines.
    // =================================================================
    for ($line_counter = 0; $line_counter < $s_processfile['line'][$file_counter]; $line_counter++) {


      // ===============================================================
      // Check which lines should be displayed.
      // ===============================================================
      $action = $s_processfile['action'][$file_counter][$line_counter];
      if (($unchanged_lines == 1 && $action == 0) || ($changed_lines == 1 && $action == 1) || ($deleted_lines == 1 && $action == 2)) {


        // =============================================================
        // Display the line.
        // =============================================================
        echo "<tr>\n";
        echo "<td class=\"file-detail-col1\">\n";
        echo $s_processfile['line_number'][$file_counter][$line_counter]."\n";
        echo "</td>\n";


        // =============================================================
        // Display the action with the right style.
        // =============================================================
        if ($s_processfile['action'][$file_counter][$line_counter] == 2) {
          echo "<td class=\"file-detail-col2-delete\">\n";
          echo stripslashes($filedetail[9])."\n";
        } elseif ($s_processfile['action'][$file_counter][$line_counter] == 1) {
          echo "<td class=\"file-detail-col2-change\">\n";
          echo stripslashes($filedetail[10])."\n";
        } elseif ($s_processfile['action'][$file_counter][$line_counter] == 0) {
          echo "<td class=\"file-detail-col2-none\">\n";
          echo stripslashes($filedetail[11])."\n";
        }
        echo "</td>\n";


        // =============================================================
        // Display the script flag with the right style.
        // =============================================================
        if ($s_processfile['js_active'][$file_counter][$line_counter] == 1) {
          echo "<td class=\"file-detail-col3-active\">\n";
          echo "J\n";
        } else {
          echo "<td class=\"file-detail-col3-normal\">\n";
          echo "&nbsp;\n";
        }
        echo "</td>\n";


        // =============================================================
        // Display the style flag with the right style.
        // =============================================================
        if ($s_processfile['css_active'][$file_counter][$line_counter] == 1) {
          echo "<td class=\"file-detail-col3-active\">\n";
          echo "C\n";
        } else {
          echo "<td class=\"file-detail-col3-normal\">\n";
          echo "&nbsp;\n";
        }
        echo "</td>\n";


        // =============================================================
        // Display the line information with the right style.
        // =============================================================
        if ($s_processfile['code_line_new'][$file_counter][$line_counter] == "&nbsp;" || $action == 2) {
          echo "<td class=\"file-detail-col4-empty\">\n";
        } else {
          echo "<td class=\"file-detail-col4-normal\">\n";
        }
        echo $s_processfile['info'][$file_counter][$line_counter]."\n";
        echo "</td>\n";


        // =============================================================
        // Display the code with the right style.
        // =============================================================
        if ($s_processfile['code_line_new'][$file_counter][$line_counter] == "&nbsp;" || $action == 2) {
          echo "<td class=\"file-detail-col5-empty\">\n";
        } else {
          echo "<td class=\"file-detail-col5-normal\">\n";
        }


        // =============================================================
        // Check if the old codeline should be displayed.
        // =============================================================
        if ($show_old_code == 1) {
          echo $s_processfile['code_line_old'][$file_counter][$line_counter]."\n";


        // =============================================================
        // Display the new codeline.
        // =============================================================
        } else {
          echo $s_processfile['code_line_new'][$file_counter][$line_counter]."\n";
        }
        echo "</td>\n";
        echo "</tr>\n";
      }
    }


    // =================================================================
    // Display the bottom row.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-detail-bottom\" colspan=\"6\">\n";
    echo "<a href=\"#top\">\n";
    echo stripslashes($filedetail[12])."\n";
    echo "</a>\n";
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the table end.
    // =================================================================
    echo "</table>\n";


    // =================================================================
    // Display a space.
    // =================================================================
    echo "<br>\n";
    echo "<br>\n";
  }
}
