<?php


// =====================================================================
// Check if a header has to be inserted.
// =====================================================================
if ($file_header == 1 && file_exists($destination_dir."/".$header_file_name)) {


  // ===================================================================
  // Read the header into a string.
  // ===================================================================
  $header_content = file_get_contents($destination_dir."/".$header_file_name);


  // ===================================================================
  // Prepare the header for a htm or html file.
  // ===================================================================
  if ($s_processfile['fileend'][$file_counter] == "htm" || $s_processfile['fileend'][$file_counter] == "html") {
    $header_content = "<!--\n".$header_content."\n-->\n\n\n";
    $header_begin = 1;
  }


  // ===================================================================
  // Prepare the header for a css or js file.
  // ===================================================================
  if ($s_processfile['fileend'][$file_counter] == "css" || $s_processfile['fileend'][$file_counter] == "js") {
    $header_content = "/*\n".$header_content."\n*/\n\n\n";
    $header_begin = 1;
  }


  // ===================================================================
  // Prepare the header for a php file.
  // ===================================================================
  if ($s_processfile['fileend'][$file_counter] == "php") {
    $header_content = "/*\n".$header_content."\n*/\n\n\n";
    $header_begin = 2;
  }
}
