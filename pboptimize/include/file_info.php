<?php


// =====================================================================
// Loop through the files.
// =====================================================================
for ($file_counter = 0; $file_counter < count($s_processfile['name']); $file_counter++) {


  // ===================================================================
  // Check if the filesize is not zero.
  // ===================================================================
  if ($s_processfile['size_b'][$file_counter] != 0) {


    // =================================================================
    // Calculate filesize in percent.
    // =================================================================
    $size_new_percent = round(($d_processfile['size_b'][$file_counter] / $s_processfile['size_b'][$file_counter] * 100), 2);
    $size_old_percent = 100 - round(($d_processfile['size_b'][$file_counter] / $s_processfile['size_b'][$file_counter] * 100), 2);
    $size_dif_percent = (100 - $size_new_percent);
    if ($size_dif_percent != 0) {
      $size_dif_percent = $size_dif_percent * -1;
    }


    // =================================================================
    // Calculate the graphics for the filesize in percent.
    // =================================================================
    $size_new_graphic = floor($d_processfile['size_b'][$file_counter] / $s_processfile['size_b'][$file_counter] * 200);
    $size_old_graphic = 200 - $size_new_graphic;


    // =================================================================
    // Calculate lines in percent.
    // =================================================================
    $line_new_percent = round(($d_processfile['line'][$file_counter] / $s_processfile['line'][$file_counter] * 100), 2);
    $line_old_percent = 100 - round(($d_processfile['line'][$file_counter] / $s_processfile['line'][$file_counter] * 100), 2);
    $line_dif_percent = (100 - $line_new_percent);
    if ($line_dif_percent != 0) {
      $line_dif_percent = $line_dif_percent * -1;
    }


    // =================================================================
    // Calculate the graphics for lines in percent.
    // =================================================================
    $line_new_graphic = floor($d_processfile['line'][$file_counter] / $s_processfile['line'][$file_counter] * 200);
    $line_old_graphic = 200 - $line_new_graphic;


  // ===================================================================
  // The filezize is zero - The file is empty.
  // ===================================================================
  } else {


    // =================================================================
    // Specify the filesize in percent.
    // =================================================================
    $size_new_percent = 100;
    $size_old_percent = 100;
    $size_dif_percent = 0;


    // =================================================================
    // Specify the graphics for the filesize in percent.
    // =================================================================
    $size_new_graphic = 200;
    $size_old_graphic = 0;


    // =================================================================
    // Specify the lines in percent.
    // =================================================================
    $line_new_percent = 100;
    $line_old_percent = 100;
    $line_dif_percent = 0;


    // =================================================================
    // Specify the graphics for lines in percent.
    // =================================================================
    $line_new_graphic = 200;
    $line_old_graphic = 0;
  }


  // ===================================================================
  // Check if empty files should be displayed.
  // ===================================================================
  if ($s_processfile['size_b'][$file_counter] != 0 || $display_empty_file == 1) {


    // =================================================================
    // Display the information table begin.
    // =================================================================
    echo "<table class=\"file-info\" border=\"0\" width=\"760\" cellspacing=\"0\" cellpadding=\"0\">\n";


    // =================================================================
    // Display the title row.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-title\" colspan=\"5\">\n";
    echo stripslashes($fileinfo[0])." ".$s_processfile['name'][$file_counter];


    // =================================================================
    // Check if the link to the file details should be displayed.
    // =================================================================
    if ($file_detail == 1 && $s_processfile['size_b'][$file_counter] != 0) {
      echo " (";
      echo "<a href=\"#".$s_processfile['name'][$file_counter]."\">";
      echo stripslashes($fileinfo[1]);
      echo "</a>";
      echo ")\n";
    }
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the header row.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-header1\">\n";
    echo stripslashes($fileinfo[2])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-header2\">\n";
    echo "<a href=\"".$link_source_dir."\" target=\"_blank\">\n";
    echo stripslashes($fileinfo[3])."\n";
    echo "</a>\n";
    echo "</td>\n";
    echo "<td class=\"file-info-header3\">\n";
    echo "<a href=\"".$link_destination_dir."\" target=\"_blank\">\n";
    echo stripslashes($fileinfo[4])."\n";
    echo "</a>\n";
    echo "</td>\n";
    echo "<td class=\"file-info-header4\">\n";
    echo stripslashes($fileinfo[5])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-header5\">\n";
    echo stripslashes($fileinfo[6])."\n";
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the data row - filesize in bytes.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-col1\">\n";
    echo stripslashes($fileinfo[7])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col2\">\n";
    echo $s_processfile['size_b'][$file_counter]."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col3\">\n";
    echo $d_processfile['size_b'][$file_counter]."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col4\">\n";
    echo $d_processfile['size_b'][$file_counter] - $s_processfile['size_b'][$file_counter]."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col5\" rowspan=\"2\">\n";
    graphic_bar ($size_new_graphic, $size_old_graphic);
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the data row - filesize in kilobytes.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-col1\">\n";
    echo stripslashes($fileinfo[8])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col2\">\n";
    echo sprintf("%01.2f", $s_processfile['size_kb'][$file_counter])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col3\">\n";
    echo sprintf("%01.2f", $d_processfile['size_kb'][$file_counter])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col4\">\n";
    echo sprintf("%01.2f", $d_processfile['size_kb'][$file_counter] - $s_processfile['size_kb'][$file_counter])."\n";
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the data row - filesize in percent.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-col1\">\n";
    echo stripslashes($fileinfo[9])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col2\">\n";
    echo "100\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col3\">\n";
    echo $size_new_percent."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col4\">\n";
    echo $size_dif_percent."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-header5\">\n";
    echo stripslashes($fileinfo[10])."\n";
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the data row - number of lines.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-col1\">\n";
    echo stripslashes($fileinfo[11])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col2\">\n";
    echo $s_processfile['line'][$file_counter]."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col3\">\n";
    echo $d_processfile['line'][$file_counter]."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col4\">\n";
    echo $d_processfile['line'][$file_counter] - $s_processfile['line'][$file_counter]."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col5\" rowspan=\"2\">\n";
    graphic_bar ($line_new_graphic, $line_old_graphic);
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the data row - number of lines in percent.
    // =================================================================
    echo "<tr>\n";
    echo "<td class=\"file-info-col1\">\n";
    echo stripslashes($fileinfo[12])."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col2\">\n";
    echo "100\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col3\">\n";
    echo $line_new_percent."\n";
    echo "</td>\n";
    echo "<td class=\"file-info-col4\">\n";
    echo $line_dif_percent."\n";
    echo "</td>\n";
    echo "</tr>\n";


    // =================================================================
    // Display the table end.
    // =================================================================
    echo "</table>\n";


    // =================================================================
    // Display a space.
    // =================================================================
    echo "<br>\n";
  }
}
