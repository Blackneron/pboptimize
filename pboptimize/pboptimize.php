<?php


// =====================================================================
// Set the maximum execution time.
// =====================================================================
ini_set('max_execution_time', 3600); // One hour


// =====================================================================
// Initialize variables.
// =====================================================================
$excludefile = array();
$filetype = array();
$s_copy = array();
$data['status'] = 0;


// =====================================================================
// Include the configuration file.
// =====================================================================
include("config/pboptimize_config.php");


// =====================================================================
// Include the stop pattern file.
// =====================================================================
include("config/stop_pattern.php");


// =====================================================================
// Include the language pattern file.
// =====================================================================
include("config/change_pattern.php");


// =====================================================================
// Include the function file.
// =====================================================================
include("include/function.php");


// =====================================================================
// Include the language file.
// =====================================================================
include("language/language.".$language);


// =====================================================================
// Include the html header.
// =====================================================================
include("include/page_header.php");


// =====================================================================
// Specify the directories for the links.
// =====================================================================
$link_source_dir = $source_dir;
$link_destination_dir = $destination_dir;


// =====================================================================
// Get the real path strings for the file functions.
// =====================================================================
$source_dir = real_path ($source_dir);
$destination_dir = real_path ($destination_dir);


// =====================================================================
// Get information from the source directory.
// =====================================================================
$status = directory_info ($source_dir, 10, $filetype, $excludefile);


// =====================================================================
// Check if the normal files have to be copied and copy them to a new
// array so that the old one can be unset.
// =====================================================================
if ($copy_files == 1) {
  $s_copy = $copyfile;
}


// =====================================================================
// If the submit button was pressed, create the destination directories
// and copy the files which not will be optimized.
// =====================================================================
if (isset($_POST['optimize'])) {
  $status = directory_create ($destination_dir, $directory, $s_copy);
}


// =====================================================================
// Copy the information to the source arrays so that the old ones can
// be unset.
// =====================================================================
$s_directory = $directory;
$s_processfile = $processfile;
$s_copyfile = $copyfile;


// =====================================================================
// Sort the source arrays.
// =====================================================================
sort_info();


// =====================================================================
// Unset the original arrays.
// =====================================================================
unset($directory);
unset($processfile);
unset($copyfile);


// =====================================================================
// Check if the files have to be optimized.
// =====================================================================
if (isset($_POST['optimize'])) {


  // ===================================================================
  // Set the start time.
  // ===================================================================
  $start_time = time();


  // ===================================================================
  // Check if the progress bar should be displayed.
  // ===================================================================
  if ($progress_bar == 1) {


    // =================================================================
    // Get the total number of lines from the actual file.
    // =================================================================
    $p_total_lines = array_sum($s_processfile['line']);


    // =================================================================
    // Calculate the progressbar values for less than 100 lines.
    // =================================================================
    if ($p_total_lines < 100) {
      $p_interval = 1;
      $p_resolution = floor(400 / $p_total_lines);
      $p_remain = 400 - $p_total_lines * $p_resolution;


    // =================================================================
    // Calculate the progressbar values for more than 100 lines.
    // =================================================================
    } else {
      $p_interval = floor($p_total_lines / 100);
      $p_remain = $p_total_lines - floor($p_total_lines / 100) * 100;
      $p_line_end = $p_total_lines - $p_remain;
    }


    // =================================================================
    // Initialize the progressbar variables.
    // =================================================================
    $p_bar_length = 1;
    $p_interval_counter = 0;


    // =================================================================
    // Progressbar begin tag.
    // =================================================================
    echo "<div class=\"progressbar-".$language."\">\n";


    // =================================================================
    // Progressbar space graphic.
    // =================================================================
    echo "<img src=\"images/pixel.gif\" width=\"98\" height=\"100\" border=\"0\" alt=\"\">";


    // =================================================================
    // Start flushing the output.
    // =================================================================
    ob_start();
  }


  // ===================================================================
  // Create the header for the files if necessary.
  // ===================================================================
  include("include/file_header.php");


  // ===================================================================
  // Optimize the files.
  // ===================================================================
  include("include/optimize.php");


  // ===================================================================
  // Check if the progress bar should be displayed.
  // ===================================================================
  if ($progress_bar == 1) {


    // =================================================================
    // End flushing the output.
    // =================================================================
    ob_end_flush();


    // =================================================================
    // Display the progressbar end tag.
    // =================================================================
    echo "</div>\n";
  }


  // ===================================================================
  // Get information from the destination directory.
  // ===================================================================
  $status = directory_info ($destination_dir, 10, $filetype, $excludefile);


  // ===================================================================
  // Copy the information to the destination arrays.
  // ===================================================================
  $d_directory = $directory;
  $d_processfile = $processfile;


  // ===================================================================
  // Sort the source arrays.
  // ===================================================================
  sort_info();


  // ===================================================================
  // Unset the original arrays.
  // ===================================================================
  unset($directory);
  unset($processfile);
  unset($copyfile);


  // ===================================================================
  // Set the end time.
  // ===================================================================
  $end_time = time();


  // ===================================================================
  // Calculate the information for the info line.
  // ===================================================================
  $file_number = count($s_processfile['name']);
  $file_lines = array_sum($s_processfile['line']);
  $time_difference = $end_time - $start_time;


  // ===================================================================
  // Calculate the minutes.
  // ===================================================================
  $time_difference_m = floor($time_difference / 60);
  $time_difference_s = $time_difference - $time_difference_m *60;


  // ===================================================================
  // Write some job information above the title.
  // ===================================================================
  echo "<span class=\"infoline\">\n";
  echo $file_number." ".stripslashes($infoline[0])." | ".$file_lines." ".stripslashes($infoline[1])." ".$time_difference_m." ".stripslashes($infoline[2])." ".$time_difference_s." ".stripslashes($infoline[3])." | ".date('d.m.Y')." / ".date('H:i:s')."\n";
  echo "</span>\n";
  echo "<br>\n";
  echo "<br>\n";
  echo "<br>\n";


  // ===================================================================
  // Display the project overview information.
  // ===================================================================
  if ($project_info == 1) {
    include("include/project_info.php");
  }


  // ===================================================================
  // Display the file information.
  // ===================================================================
  if ($file_info == 1) {
    include("include/file_info.php");
  }


  // ===================================================================
  // Display the file detail information.
  // ===================================================================
  if ($file_detail == 1) {
    include("include/file_detail.php");
  }


// =====================================================================
// If the submit button was not pressed display the begin.
// =====================================================================
} else {


  // ===================================================================
  // Display the main table.
  // ===================================================================
  include("include/maintable.php");
}


// =====================================================================
// Include the html footer.
// =====================================================================
include("include/page_footer.php");
