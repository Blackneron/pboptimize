<?php


// =====================================================================
// Specify the number of patterns.
// =====================================================================
$stop_number = 20;


// =====================================================================
// 0 - Stop patterns for all modifications.
// =====================================================================


    // =================================================================
    // Matches the string "strpos", "preg" or "ereg" but only if there
    // are not two // characters in front (comment).
    // =================================================================
    $stop_pattern[0][] = '/^(?:(?!\\/\\/).)*(?:str[i]{0,1}pos|preg|ereg|replace).*$/i';
    $stop_process[0][] = "match";
    $stop_message[0][] = "STOP-PATTERN";


// =====================================================================
// 1 - Stop patterns for detecting javascript sections.
// =====================================================================


    // =================================================================
    // Matches a script tag inside a php single line comment.
    // =================================================================
    $stop_pattern[1][] = '/^.*\/\/.*(?:<[\/]{0,1}script>)/i';
    $stop_process[1][] = "match";
    $stop_message[1][] = "STOP-SCRIPT";


// =====================================================================
// 2 - Stop patterns for detecting css stylesheet sections.
// =====================================================================


    // =================================================================
    // Matches a style tag inside a php single line comment.
    // =================================================================
    $stop_pattern[2][] = '/^.*\/\/.*(?:<[\/]{0,1}style)/i';
    $stop_process[2][] = "match";
    $stop_message[2][] = "STOP-STYLE";


// =====================================================================
// 3 - Stop patterns for deleting html multi line comments.
// =====================================================================


    // =================================================================
    // Matches if a tag for the zoom search engine was found
    // =================================================================
    $stop_pattern[3][] = '/<!--ZOOM(?:STOP|RESTART|SEARCH){1}-->/m';
    $stop_process[3][] = "match";
    $stop_message[3][] = "STOP-ZOOM";


    // =================================================================
    // Matches if a html comment begin tag was found but it is inside a
    // php comment.
    // =================================================================
    $stop_pattern[3][] = '/\\/\\/.*?<!--/m';
    $stop_process[3][] = "match";
    $stop_message[3][] = "STOP-HTML-BEGIN";


// =====================================================================
// 7 - Stop patterns for deleting html single line comments.
// =====================================================================


    // =================================================================
    // Matches if a tag for the zoom search engine was found
    // =================================================================
    $stop_pattern[7][] = '/<!--ZOOM(?:STOP|RESTART|SEARCH){1}-->/m';
    $stop_process[7][] = "match";
    $stop_message[7][] = "STOP-ZOOM";


    // =================================================================
    // Matches if a html comment begin tag was found but it is
    // inside a php comment.
    // =================================================================
    $stop_pattern[7][] = '/\\/\\/.*?<!--/m';
    $stop_process[7][] = "match";
    $stop_message[7][] = "STOP-HTML-BEGIN";


// =====================================================================
// 8 - Stop patterns for deleting php single line comments.
// =====================================================================


    // =================================================================
    // Matches a http address.
    // =================================================================
    $stop_pattern[8][] = '/^[^\\/]{2}.*https?:\\/\\//i';
    $stop_process[8][] = "match";
    $stop_message[8][] = "STOP-HTTP";


    // =================================================================
    // Matches a html document declaration
    // =================================================================
    $stop_pattern[8][] = '/<!DOCTYPE HTML/i';
    $stop_process[8][] = "match";
    $stop_message[8][] = "STOP-DOCTYPE";


    // =================================================================
    // Matches a comment which is inside a text string.
    // =================================================================
    $stop_pattern[8][] = '/"\/\//';
    $stop_process[8][] = "match";
    $stop_message[8][] = "STOP-TEXT";


    // =================================================================
    // Matches a double slash at the end of a regex pattern.
    // =================================================================
    $stop_pattern[8][] = '/=\\ {0,}\'\/.*\/[a-z]{0,3}\'/';
    $stop_process[8][] = "match";
    $stop_message[8][] = "STOP-TEXT";


// =====================================================================
// 9 - Stop patterns for deleting javascript single line comments.
// =====================================================================


    // =================================================================
    // Matches a http address.
    // =================================================================
    $stop_pattern[9][] = '/^[^\/]*?https?:\/\//i';
    $stop_process[9][] = "match";
    $stop_message[9][] = "STOP-HTTP";


// =====================================================================
// 10 - Stop patterns for deleting css single line comments.
// =====================================================================


    // =================================================================
    // Matches a http address.
    // =================================================================
    $stop_pattern[10][] = '/^[^\/]*?https?:\/\//i';
    $stop_process[10][] = "match";
    $stop_message[10][] = "STOP-HTTP";
