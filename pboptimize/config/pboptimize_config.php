<?php


// =====================================================================
// Specify the application language. Available is german "ge" and
// english "en".
// =====================================================================
$language = "en";


// =====================================================================
// Specify the optimizing options.
// =====================================================================
$js_detect           = 1; //  1 - Detect javascript areas
$css_detect          = 1; //  2 - Detect CSS areas
$html_multi_comment  = 1; //  3 - HTML multi line comments
$php_multi_comment   = 1; //  4 - PHP multi line comments
$js_multi_comment    = 1; //  5 - Javascript multi line comments
$css_multi_comment   = 1; //  6 - CSS multi line comments
$html_single_comment = 1; //  7 - HTML single line comments
$php_single_comment  = 1; //  8 - PHP single line comments
$js_single_comment   = 1; //  9 - Javascript single line comments
$css_single_comment  = 1; // 10 - CSS single line comments
$trim_left           = 1; // 11 - Trim left side of the line
$trim_right          = 1; // 12 - Trim right side of the line
$normal_line_feed    = 1; // 13 - Remove normal line feeds and carriage returns !
$empty_line          = 1; // 14 - Delete empty line
$js_line_feed        = 1; // 15 - Remove javascript line feeds and carriage returns
$replace_tabulator   = 1; // 16 - Replaces tabulators with spaces
$all_remove_string   = 1; // 17 - Remove strings in all areas
$css_remove_string   = 1; // 18 - Remove strings in css areas
$js_remove_string    = 1; // 19 - Remove strings in js areas
$css_line_feed       = 1; // 20 - Remove css line feeds and carriage returns !


// =====================================================================
// Specify the display options.
// =====================================================================
$project_info    = 1;  // Display the project info table
$file_info       = 0;  // Display the file info table - creates a lot of output !
$file_detail     = 0;  // Display the file detail table - creates a lot of output !
$unchanged_lines = 0;  // Display the unchanged codelines
$changed_lines   = 0;  // Display the changed codelines
$deleted_lines   = 0;  // Display the deleted codelines
$show_old_code   = 0;  // Display the show the unmodified code
$line_chars      = 76; // Number of chars in the codeline (76 IE / 88 Firefox)
$progress_bar    = 1;  // Display the progressbar


// =====================================================================
// Specify the fileendings for the files to be optimized.
// =====================================================================
$filetype[] = "html"; // HTML file
$filetype[] = "htm";  // HTML file
$filetype[] = "php";  // PHP file
$filetype[] = "php3"; // PHP file
$filetype[] = "js";   // Javascript file
$filetype[] = "css";  // Cascading stylesheet file


// =====================================================================
// Specify the file exeptions (path inside the source directory).
// =====================================================================
$excludefile[] = "config.php";


// =====================================================================
// Specify the directory names like "source" or "destination".
// =====================================================================
$source_dir = "source";
$destination_dir = "destination";


// =====================================================================
// Specify if the normal files have to be copied to the destination
// directory.
// =====================================================================
$copy_files = 1;


// =====================================================================
// Specify if empty and for optimizing selected files should be created
// in the destination directory.
// =====================================================================
$create_empty_files = 1;


// =====================================================================
// Specify if information about empty files should be displayed.
// =====================================================================
$display_empty_file = 0;


// =====================================================================
// Specify if a file header should be inserted.
// =====================================================================
$file_header = 0;


// =====================================================================
// Specify the header filename in the source directory.
// =====================================================================
$header_file_name = "header.pbo";


// =====================================================================
// Specify the fileendings for javascript files.
// =====================================================================
$javascript_fileending[] = "js";


// =====================================================================
// Specify the fileendings for cascading stylesheet files.
// =====================================================================
$css_fileending[] = "css";


// =====================================================================
// Display code indent.
// =====================================================================
$code_indent = 1;


// =====================================================================
// Tabulator size for the code indent.
// =====================================================================
$tabulator_size = 2;


// =====================================================================
// Specify the exeption range for the all option matches.
// =====================================================================
$all_option_exeption[] = 11;
$all_option_exeption[] = 12;
$all_option_exeption[] = 13;
$all_option_exeption[] = 14;
$all_option_exeption[] = 15;
