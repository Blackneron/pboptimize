# PBoptimize - README #
---

### Overview ###

The **PBoptimize** web application can minimize php, css and js files.

### Screenshots ###

![PBoptimize - Start screen](development/readme/pboptimize1.png "PBoptimize - Start screen")

![PBoptimize - Project and file info](development/readme/pboptimize2.png "PBoptimize - Project and file info")

![PBoptimize - Job protocol](development/readme/pboptimize3.png "PBoptimize - Job protocol")

### Setup ###

* Upload the directory **pboptimize** to your webhost.
* Edit the configuration file **pboptimize/config/pboptimize_config.php**.
* Copy the files to be optimized to the folder **pboptimize/source**.
* Display the start page **pboptimize/pboptimize.php**.
* Press the button **Optimize Files !** to start the optimization process.
* Get the optimized files from the folder **pboptimize/destination**.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBoptimize** web application is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
